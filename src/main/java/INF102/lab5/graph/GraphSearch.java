package INF102.lab5.graph;

import java.util.HashSet;
import java.util.Set;
/**
 * This class is used to conduct search algorithms of a graph
 */
public class GraphSearch<V> implements IGraphSearch<V> {

    private IGraph<V> graph;

    public GraphSearch(IGraph<V> graph) {
        this.graph = graph;
    }

    @Override
    public boolean connected(V u, V v) {
        Set<V> traversed = new HashSet<>();
        traversed = traverse(u, traversed);
        return traversed.contains(v);
    }

    public Set<V> traverse(V u, Set<V> traversed){
        for (V node : graph.getNeighbourhood(u)) {
            if (!traversed.contains(node)){
                traversed.add(node);
                traversed = traverse(node, traversed);
            }
        }
        return traversed;
    }
}
