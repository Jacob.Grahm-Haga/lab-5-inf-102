package INF102.lab5.graph;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AdjacencySet<V> implements IGraph<V> {

    private Set<V> nodes;
    private Map<V, Set<V>> nodeToNode;

    public AdjacencySet() {
        nodes = new HashSet<>();
        nodeToNode = new HashMap<>();
    }

    @Override
    public int size() {
        return nodes.size();
    }

    @Override
    public Set<V> getNodes() {
        return Collections.unmodifiableSet(nodes);
    }

    @Override
    public void addNode(V node) {
        if ((hasNode(node))){
            return;
        }
        nodes.add(node);
        nodeToNode.put(node, new HashSet<V>());
    }

    @Override
    public void removeNode(V node) {
        for (V edgeNode :getNeighbourhood(node)) {
            removeDirectedEdge(edgeNode, node);
        }
        nodeToNode.remove(node);
        nodes.remove(node);
    }

    @Override
    public void addEdge(V u, V v) {
        if ((!hasNode(u)) || (!hasNode(v))){
            throw new IllegalArgumentException("You can't add an edge to a node that doesn't exist on the graph.");
        }
        addDirectedEdge(u, v);
        addDirectedEdge(v, u);
    }

    public void addDirectedEdge(V u, V v) {
        Set<V> uSet = new HashSet<>();
        if (nodeToNode.get(u) != null){
            uSet = nodeToNode.get(u);
        }
        uSet.add(v);
        nodeToNode.put(u, uSet);
    }

    @Override
    public void removeEdge(V u, V v) {
        if ((!hasEdge(u, v)) || (!hasEdge(v, u))){
           throw new NullPointerException();
        }
        removeDirectedEdge(u, v);
        removeDirectedEdge(v, u);
    }

    public void removeDirectedEdge(V u, V v) {
        nodeToNode.remove(u, nodeToNode.get(u).remove(v));
    }

    @Override
    public boolean hasNode(V node) {
        return nodes.contains(node);
    }

    @Override
    public boolean hasEdge(V u, V v) {
        return nodeToNode.get(u).contains(v);
    }

    @Override
    public Set<V> getNeighbourhood(V node) {
        return Collections.unmodifiableSet(nodeToNode.get(node));
    }

    @Override
    public String toString() {
        StringBuilder build = new StringBuilder();
        for (V node : nodes) {
            Set<V> nodeList = nodeToNode.get(node);

            build.append(node);
            build.append(" --> ");
            build.append(nodeList);
            build.append("\n");
        }
        return build.toString();
    }

}
